﻿#ifndef GBASECLASS1_H
#define GBASECLASS1_H

#include <QObject>
#include <opencv2/core.hpp>

class GBaseClass : public QObject
{
    Q_OBJECT
public:
    explicit GBaseClass(QObject *parent = nullptr);

signals:
    bool ShowImageSig(const cv::Mat&, const std::string);

public slots:
};

#endif // GBASECLASS1_H
