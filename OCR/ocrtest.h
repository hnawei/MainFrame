﻿#ifndef OCRTEST_H
#define OCRTEST_H

#include <QDialog>
#include <opencv2/core.hpp>

namespace Ui {
class OcrTest;
}

class OcrTest :  public QDialog
{
    Q_OBJECT

public:
    explicit OcrTest(QWidget *parent = nullptr);
    ~OcrTest();

    void SplitContours(int SplitContours_k, double SplitContours_t);

public slots:
    void SlectectImageToShowSlot(const std::string ImageId, const std::string objectName);

private slots:
    void on_pushButton_clicked();
    void on_OpenImage_clicked();
    void on_GetPoint_clicked();
    void on_SplitContours_clicked();
    void on_SplitContours_k_valueChanged(int arg1);
    void on_SplitContours_t_valueChanged(double arg1);

    void on_Ed_Test_clicked();

    void on_ShapeMatch_clicked();

    void on_ShapeTrain_clicked();

    void on_RectifyPurcC_clicked();

    void on_RectifyOpencvEdge_clicked();

    void on_RectifyOpencvLine_clicked();

    void on_btnSetInputImage_clicked();

    void on_btnSetOutputImage_clicked();

signals:
    bool ShowImageSig(const cv::Mat&, const std::string);
    void UpdateIconListSig(const std::map<std::string, cv::Mat>, const std::string objectName);

private:
    void RectifyImageWithText( int type );

private:
    Ui::OcrTest *ui;

    //cv::Mat image;
    //std::vector<cv::Mat> procImgs;

    QString ImagePath;
    std::map<std::string, cv::Mat> ProcImageMap;
};

#endif // OCRTEST_H
