﻿#ifndef TOLUA_H
#define TOLUA_H

#include <opencv2/features2d.hpp>

#ifdef __WITH_LUA__

#define __ENABLE_SOL__ 1

#if __ENABLE_SOL__

#include "lua.hpp"
#define SOL_ALL_SAFETIES_ON 1
#include "sol/sol.hpp"
extern sol::state luaState;

#else

#include "kaguya/kaguya.hpp"
extern kaguya::State luaState;

#endif

#endif

bool InitThisLua();

bool initFun_Lua();

cv::Ptr<cv::FeatureDetector> GetDector_Lua( const std::string lines);


#endif // TOLUA_H
