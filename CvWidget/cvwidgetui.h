﻿#ifndef CVWIDGETUI_H
#define CVWIDGETUI_H

#include <atomic>
#include <QFrame>
#include <opencv2/core.hpp>

namespace Ui {
class cvwidgetui;
}

class cvwidgetui : public QFrame
{
    Q_OBJECT

public:
    explicit cvwidgetui(QWidget *parent = nullptr);
    ~cvwidgetui();

    void ShowImage(bool JustShow, cv::Mat *pImage);
    void fnShowRoi( std::string id );

private slots:
    void on_DirTestButton_clicked(bool checked);
    void on_DrawLine_clicked();
    void on_verticality_clicked();
    void on_OpenImage_clicked();
    void on_ImageList_currentTextChanged(const QString &currentText);

public slots:
    bool ShowImageSlot(const cv::Mat&, const std::string Name);
    void UpdateIconListSlot(const std::map<std::string, cv::Mat>, const std::string objectName);

    bool AddImageSlot(const cv::Mat&, const std::string Name);

signals:
    void SlectectImageToShowSig(const std::string ImageId, const std::string objectName);

private:
    Ui::cvwidgetui *ui;

    cv::Mat m_Image;
    std::atomic_int Runing;

    std::map<std::string, QImage> m_QImageList;
    std::map<std::string, cv::Mat> m_OrgImageList;
    std::string CurrentObiectName;
};

#endif // CVWIDGETUI_H
