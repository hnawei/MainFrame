#-------------------------------------------------
#
# Project created by QtCreator 2019-05-29T19:26:22
#
#-------------------------------------------------

QT       += core gui testlib

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = MainFrame
TEMPLATE = app

# The following define makes your compiler emit warnings if you use
# any feature of Qt which has been marked as deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x056000    # disables all the APIs deprecated before Qt 6.0.0
DEFINES += _USE_QT_
#DEFINES += __WITH_CHAISCRIPT__
#DEFINES += __WITH_LUA__

#CONFIG += c++17
CONFIG += c++1z

include (../deskew-master/Rectify/Rectify.pri)

SOURCES += \
        CvWidget/cvwidgetui.cpp \
        ED_Lib-master/ED.cpp \
        ED_Lib-master/EDCircles.cpp \
        ED_Lib-master/EDColor.cpp \
        ED_Lib-master/EDLines.cpp \
        ED_Lib-master/EDPF.cpp \
        ED_Lib-master/NFA.cpp \
        OCR/PassImageProc.cpp \
        OCR/ocrtest.cpp \
        QtOpenCV/cvmatandqimage.cpp \
        FeatureDetection/featuredetector.cpp \
        Script/ChaiScriptSyntaxHighlighter.cpp \
        Script/chaiscript.cpp \
        Script/tolua.cpp\
        gbaseclass.cpp \
        main.cpp \
        mainui.cpp  \
        msghandlerwapper.cpp \
        shape_based_matching/line2Dup.cpp

HEADERS += \
        CvWidget/cvwidgetui.h \
        ED_Lib-master/ED.h \
        ED_Lib-master/EDCircles.h \
        ED_Lib-master/EDColor.h \
        ED_Lib-master/EDLib.h \
        ED_Lib-master/EDLines.h \
        ED_Lib-master/EDPF.h \
        ED_Lib-master/NFA.h \
        OCR/PassImageProc.h \
        OCR/ocrtest.h \
        QtOpenCV/asmopencv.h \
        QtOpenCV/cvmatandqimage.h \
        FeatureDetection/featuredetector.h \
        Script/ChaiScriptSyntaxHighlighter.hpp \
        Script/chaiscript.h \
        Script/tolua.h\
        gbaseclass.h \
        mainui.h  \
        msghandlerwapper.h \
        shape_based_matching/MIPP/math/avx512_mathfun.h \
        shape_based_matching/MIPP/math/avx512_mathfun.hxx \
        shape_based_matching/MIPP/math/avx_mathfun.h \
        shape_based_matching/MIPP/math/avx_mathfun.hxx \
        shape_based_matching/MIPP/math/neon_mathfun.h \
        shape_based_matching/MIPP/math/neon_mathfun.hxx \
        shape_based_matching/MIPP/math/sse_mathfun.h \
        shape_based_matching/MIPP/math/sse_mathfun.hxx \
        shape_based_matching/MIPP/mipp.h \
        shape_based_matching/MIPP/mipp_impl_AVX.hxx \
        shape_based_matching/MIPP/mipp_impl_AVX512.hxx \
        shape_based_matching/MIPP/mipp_impl_NEON.hxx \
        shape_based_matching/MIPP/mipp_impl_SSE.hxx \
        shape_based_matching/MIPP/mipp_object.hxx \
        shape_based_matching/MIPP/mipp_scalar_op.h \
        shape_based_matching/MIPP/mipp_scalar_op.hxx \
        shape_based_matching/line2Dup.h

FORMS += \
        CvWidget/cvwidgetui.ui \
        FeatureDetection/featuredetector.ui \
        mainui.ui \
        OCR/ocrtest.ui

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target

win32:!win32-g++: {
message("Using win32 configuration")

OPENCV_HEADER = $(OPENCV_INCLUDE) # Note: update with the correct OpenCV version
QMAKE_CXXFLAGS += /bigobj
LIBS_PATH = $(OPENCV_LIB) #project compiled using Visual C++ 2010 32bit compiler

    CONFIG(debug, debug|release) {
    LIBS     += -L$$LIBS_PATH \
    -lopencv_bgsegm343d\
    -lopencv_bioinspired343d\
    -lopencv_calib3d343d\
    -lopencv_ccalib343d\
    -lopencv_core343d\
    -lopencv_datasets343d\
    -lopencv_dnn343d\
    -lopencv_dnn_objdetect343d\
    -lopencv_dpm343d\
    -lopencv_face343d\
    -lopencv_features2d343d\
    -lopencv_flann343d\
    -lopencv_fuzzy343d\
    -lopencv_hfs343d\
    -lopencv_highgui343d\
    -lopencv_imgcodecs343d\
    -lopencv_imgproc343d\
    -lopencv_img_hash343d\
    -lopencv_line_descriptor343d\
    -lopencv_ml343d\
    -lopencv_objdetect343d\
    -lopencv_optflow343d\
    -lopencv_phase_unwrapping343d\
    -lopencv_photo343d\
    -lopencv_plot343d\
    -lopencv_reg343d\
    -lopencv_rgbd343d\
    -lopencv_saliency343d\
    -lopencv_shape343d\
    -lopencv_stereo343d\
    -lopencv_stitching343d\
    -lopencv_structured_light343d\
    -lopencv_superres343d\
    -lopencv_surface_matching343d\
    -lopencv_text343d\
    -lopencv_video343d\
    -lopencv_videoio343d\
    -lopencv_videostab343d\
    -lopencv_xfeatures2d343d\
    -lopencv_ximgproc343d\
    -lopencv_xobjdetect343d\
    -lopencv_xphoto343d

    LIBS     += -L$$PWD/CvImageShow/Debug -lQtCvImageShow
    }else{
    LIBS     += -L$$LIBS_PATH \
    -lopencv_bgsegm343\
    -lopencv_bioinspired343\
    -lopencv_calib3d343\
    -lopencv_ccalib343\
    -lopencv_core343\
    -lopencv_datasets343\
    -lopencv_dnn343\
    -lopencv_dnn_objdetect343\
    -lopencv_dpm343\
    -lopencv_face343\
    -lopencv_features2d343\
    -lopencv_flann343\
    -lopencv_fuzzy343\
    -lopencv_hfs343\
    -lopencv_highgui343\
    -lopencv_imgcodecs343\
    -lopencv_imgproc343\
    -lopencv_img_hash343\
    -lopencv_line_descriptor343\
    -lopencv_ml343\
    -lopencv_objdetect343\
    -lopencv_optflow343\
    -lopencv_phase_unwrapping343\
    -lopencv_photo343\
    -lopencv_plot343\
    -lopencv_reg343\
    -lopencv_rgbd343\
    -lopencv_saliency343\
    -lopencv_shape343\
    -lopencv_stereo343\
    -lopencv_stitching343\
    -lopencv_structured_light343\
    -lopencv_superres343\
    -lopencv_surface_matching343\
    -lopencv_text343\
    -lopencv_video343\
    -lopencv_videoio343\
    -lopencv_videostab343\
    -lopencv_xfeatures2d343\
    -lopencv_ximgproc343\
    -lopencv_xobjdetect343\
    -lopencv_xphoto343

    LIBS     += -L$$PWD/CvImageShow/Release -lQtCvImageShow
    }
}

win32-g++: {
    message("Using win32-g++ configuration")

    QMAKE_CXXFLAGS += -Wa,-mbig-obj
    #QMAKE_LFLAGS += -Wa,-mbig-obj

    #QMAKE_CXXFLAGS_DEBUG += -O1

    MINGW64_PATH = D:/install/msys64/mingw64
    contains(QT_ARCH, i386) {
            OpencvRoot = $$MsysPath/opencv_349_xp
        }else{
            OpencvRoot = $$MINGW64_PATH/opencv_411_static
            #OpencvRoot = $$MINGW64_PATH/opencv-450-gcc-x64-static
        }

    OPENCV_HEADER = $$OpencvRoot/include
    OPENCV_LIBS = $$OpencvRoot/x64/mingw/staticlib

    OPENCV_VERSION_SUB =

    #message("LIBS_PATH:" $$LIBS_PATH)
    LIBS     += -L$$OPENCV_LIBS \
    -lopencv_aruco$$OPENCV_VERSION_SUB \
    -lopencv_bgsegm$$OPENCV_VERSION_SUB \
    -lopencv_bioinspired$$OPENCV_VERSION_SUB \
    -lopencv_calib3d$$OPENCV_VERSION_SUB \
    -lopencv_ccalib$$OPENCV_VERSION_SUB \
    -lopencv_datasets$$OPENCV_VERSION_SUB \
    -lopencv_dnn$$OPENCV_VERSION_SUB \
    -lopencv_dnn_objdetect$$OPENCV_VERSION_SUB \
    -lopencv_dpm$$OPENCV_VERSION_SUB \
    -lopencv_face$$OPENCV_VERSION_SUB \
    -lopencv_features2d$$OPENCV_VERSION_SUB \
    -lopencv_flann$$OPENCV_VERSION_SUB \
    -lopencv_fuzzy$$OPENCV_VERSION_SUB \
    -lopencv_hfs$$OPENCV_VERSION_SUB \
    -lopencv_img_hash$$OPENCV_VERSION_SUB \
    -lopencv_line_descriptor$$OPENCV_VERSION_SUB \
    -lopencv_ml$$OPENCV_VERSION_SUB \
    -lopencv_objdetect$$OPENCV_VERSION_SUB \
    -lopencv_optflow$$OPENCV_VERSION_SUB \
    -lopencv_phase_unwrapping$$OPENCV_VERSION_SUB \
    -lopencv_photo$$OPENCV_VERSION_SUB \
    -lopencv_plot$$OPENCV_VERSION_SUB \
    -lopencv_reg$$OPENCV_VERSION_SUB \
    -lopencv_rgbd$$OPENCV_VERSION_SUB \
    -lopencv_saliency$$OPENCV_VERSION_SUB \
    -lopencv_shape$$OPENCV_VERSION_SUB \
    -lopencv_stereo$$OPENCV_VERSION_SUB \
    -lopencv_stitching$$OPENCV_VERSION_SUB \
    -lopencv_structured_light$$OPENCV_VERSION_SUB \
    -lopencv_superres$$OPENCV_VERSION_SUB \
    -lopencv_surface_matching$$OPENCV_VERSION_SUB \
    -lopencv_text$$OPENCV_VERSION_SUB \
    -lopencv_tracking$$OPENCV_VERSION_SUB \
    -lopencv_video$$OPENCV_VERSION_SUB \
    -lopencv_videoio$$OPENCV_VERSION_SUB \
    -lopencv_videostab$$OPENCV_VERSION_SUB \
    -lopencv_xfeatures2d$$OPENCV_VERSION_SUB \
    -lopencv_ximgproc$$OPENCV_VERSION_SUB \
    -lopencv_xobjdetect$$OPENCV_VERSION_SUB \
    -lopencv_xphoto$$OPENCV_VERSION_SUB \
    -lopencv_highgui$$OPENCV_VERSION_SUB \
    -lopencv_imgcodecs$$OPENCV_VERSION_SUB \
    -lopencv_imgproc$$OPENCV_VERSION_SUB \
    -lopencv_core$$OPENCV_VERSION_SUB \
    $$OPENCV_LIBS/libIlmImf.a\
    $$OPENCV_LIBS/liblibjpeg-turbo.a\
    $$OPENCV_LIBS/liblibpng.a\
    $$OPENCV_LIBS/liblibprotobuf.a\
    $$OPENCV_LIBS/liblibtiff.a\
    $$OPENCV_LIBS/liblibwebp.a\
    $$OPENCV_LIBS/libquirc.a\
    $$OPENCV_LIBS/libzlib.a\
    -lgdi32 -lcomdlg32

    LIBS     += -L$$OPENCV_LIBS \
        $$OpencvRoot/x64/mingw/staticlib/liblibjasper.a

    #-lopencv_alphamat$$OPENCV_VERSION_SUB \
#    -lopencv_intensity_transform$$OPENCV_VERSION_SUB \
#    -lopencv_dnn_superres$$OPENCV_VERSION_SUB \
#    -lopencv_gapi$$OPENCV_VERSION_SUB \
#    -lopencv_mcc$$OPENCV_VERSION_SUB \
#    -lopencv_quality$$OPENCV_VERSION_SUB \
#    -lopencv_rapid$$OPENCV_VERSION_SUB \
    #$$OPENCV_LIBS/libade.a\
    #$$OPENCV_LIBS/liblibopenjp2.a\

    CONFIG(debug, debug|release) {
        LIBS     += $$PWD/CvImageShow/win32-g++/Debug/libQtCvImageShow.a
        #LIBS     += -L$$PWD/CvImageShow/win32-g++/Debug -lQtCvImageShow #-lQtCvImageShow.dll
    }else{
        LIBS     += $$PWD/CvImageShow/win32-g++/Release/libQtCvImageShow.a
        #LIBS     += -L$$PWD/CvImageShow/win32-g++/Release -lQtCvImageShow #-lQtCvImageShow.dll
    }

    LIBS     += -L$$MINGW64_PATH/Simd/lib -lSimd
    INCLUDEPATH += $$MINGW64_PATH/Simd/include
    DEFINES +=SIMD_STATIC

    #INCLUDEPATH += $$MINGW64_PATH/chaiscript/include
    #LIBS     += -L$$MINGW64_PATH/chaiscript/lib/chaiscript -llibchaiscript_stdlib-6.1.0  -llibstl_extra -llibtest_module

    INCLUDEPATH += $$MINGW64_PATH/luajit/include/luajit-2.0
    LIBS     += -L$$MINGW64_PATH/luajit/lib -lluajit-5.1
}

unix : {
message("Using unix configuration")

OPENCV_HEADER = /usr/include/opencv4/
LIBS_PATH = /usr/lib/

    if(contains(DEFINES,__WITH_LUA__)){
        message('-->>(DEFINES,__WITH_LUA__)')
        INCLUDEPATH += /usr/include/luajit-2.1
        LIBS     +=  /usr/lib/x86_64-linux-gnu/libluajit-5.1.a
    } else {
        message('-->>(DEFINES,__WITH_LUA__) nothing')
    }

LIBS     += -L$$LIBS_PATH \
            -lopencv_aruco\
            -lopencv_bgsegm\
            -lopencv_bioinspired\
            -lopencv_calib3d\
            -lopencv_ccalib\
            -lopencv_core\
            -lopencv_cvv\
            -lopencv_datasets\
            -lopencv_dnn\
            -lopencv_dnn_objdetect\
            -lopencv_dpm\
            -lopencv_face\
            -lopencv_features2d\
            -lopencv_flann\
            -lopencv_fuzzy\          
            -lopencv_hfs\
            -lopencv_highgui\
            -lopencv_img_hash\
            -lopencv_imgcodecs\
            -lopencv_imgproc\
            -lopencv_line_descriptor\
            -lopencv_ml\
            -lopencv_objdetect\
            -lopencv_optflow\
            -lopencv_phase_unwrapping\
            -lopencv_photo\
            -lopencv_plot\
            -lopencv_reg\
            -lopencv_rgbd\
            -lopencv_saliency\
            -lopencv_shape\
            -lopencv_stereo\
            -lopencv_stitching\
            -lopencv_structured_light\
            -lopencv_superres\
            -lopencv_surface_matching\
            -lopencv_text\
            -lopencv_tracking\
            -lopencv_video\
            -lopencv_videoio\
            -lopencv_videostab\
            -lopencv_xfeatures2d\
            -lopencv_ximgproc\
            -lopencv_xobjdetect\
            -lopencv_xphoto
#            -lopencv_gapi\
#            -lopencv_quality\

    CONFIG(debug, debug|release) {
        LIBS     += -L$$PWD/CvImageShow/g++/Debug -lQtCvImageShow       
    }else{
        LIBS     += -L$$PWD/CvImageShow/g++/Release -lQtCvImageShow
    }

    message("LIBS: $$LIBS")
    LIBS     += -ldl
    LIBS     += -L$$LIBS_PATH/chaiscript/ -lchaiscript_stdlib-6.1.0  -lstl_extra
}

INCLUDEPATH += \
    $$OPENCV_HEADER \ #core module
    $$PWD/CvImageShow/include

message("OpenCV path: $$OPENCV_PATH")
message("Includes path: $$INCLUDEPATH")
message("Libraries: $$LIBS")
