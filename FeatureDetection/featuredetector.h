﻿#ifndef FEATUREDETECTOR_H
#define FEATUREDETECTOR_H

#include <QWidget>

#include <vector>
#include <opencv2/features2d.hpp>
#include <opencv2/xfeatures2d.hpp>

namespace Ui {
class FeatureDetector;
}

class FeatureDetect : public QWidget
{
    Q_OBJECT

public:
    explicit FeatureDetect(QWidget *parent = nullptr);
    ~FeatureDetect();

private slots:
    void on_OpenImage_clicked();

    void on_FeatureType_currentIndexChanged(const QString &arg1);

    void on_ScriptExe_clicked();

    void on_EngineType_currentTextChanged(const QString &arg1);

    void on_FeatureType_currentTextChanged(const QString &arg1);

public:
    int ApplyDetect_Cpp( const std::string typeString );
    int ApplyDetect_ChaiScript( const std::string ScriptData );
    int ApplyDetect_Lua( const std::string ScriptData );

signals:
    bool ShowImageSig(const cv::Mat&, const std::string);
    void UpdateIconListSig(const std::map<std::string, cv::Mat>, const std::string objectName);

public slots:
    void SlectectImageToShowSlot(const std::string ImageId, const std::string objectName);

private:
    Ui::FeatureDetector *ui;

    cv::Mat image;
    QString ImagePath;
    std::map<std::string, cv::Mat> ProcImageMap;

    cv::Ptr<cv::FeatureDetector> detector;
    std::vector<cv::KeyPoint> mKeypoints;

    enum EngineTypeEnum{
        engine_ChaiScript,
        engine_Cpp,
        engine_Lua,
    };

    EngineTypeEnum m_EngineType;
};

#endif // FEATUREDETECTOR_H
