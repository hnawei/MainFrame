#pragma once

#include "ImageShow/CvImageShow.h"
#include "ImageShow/Shapes.h"

namespace CvImageShow {
	namespace Shapes {
		class CShapeLine : public CShape
		{
		public:
			CShapeLine(const ShapeDef& param);

			virtual int IsSelected(const cv::Point& ptPos, float fScale) override;

			virtual bool SetPointsIsOk(const std::vector<cv::Point2f>& vPoints) override;
			std::vector<cv::Point2f>& GetPoints() { return m_vPtCorners; }


#ifdef _USE_QT_
			void DrawSelf(QPainter* pPainter, const cv::Point2f& ptOffset, const float fScaleRatio);
#else
			void DrawSelf(Gdiplus::Graphics* pGdiPainter, const cv::Point2f& ptOffset, const float fScaleRatio);
#endif
			virtual void AdjustDragDelta(const cv::Point2f& ptDelta) override;

		private:
			void SetPoints(const std::vector<cv::Point2f>& vPoints);
		};
	}
}

