#pragma once

#include "ImageShow/CvImageShow.h"
#include "ImageShow/Shapes.h"
namespace CvImageShow {
	namespace Shapes {
		class CShapeCircle :
			public CShape
		{
		public:
			CShapeCircle(const ShapeDef& param);

			virtual int IsSelected(const cv::Point& ptPos, float fScale) override;

			virtual bool SetPointsIsOk(const std::vector<cv::Point2f>& vPoints) override;
			std::vector<cv::Point2f>& GetPoints() { return m_vPtSets; }

#ifdef _USE_QT_
			void DrawSelf(QPainter* pGdiPainter, const cv::Point2f& ptOffset, const float fScaleRatio);
#else
			void DrawSelf(Gdiplus::Graphics* pGdiPainter, const cv::Point2f& ptOffset, const float fScaleRatio);
#endif
			virtual void AdjustDragDelta(const cv::Point2f& ptDelta) override;

		private:
			void SetPoints(const std::vector<cv::Point2f>& vPoints);
			
			std::vector<cv::Point2f> m_vPtSets;
		};

	}
}

