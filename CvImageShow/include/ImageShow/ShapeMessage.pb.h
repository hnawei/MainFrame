// Generated by the protocol buffer compiler.  DO NOT EDIT!
// source: ShapeMessage.proto
#pragma once

#ifndef PROTOBUF_ShapeMessage_2eproto__INCLUDED
#define PROTOBUF_ShapeMessage_2eproto__INCLUDED

#ifdef _SUPPORT_PROTO_
#include <string>

#include <google/protobuf/stubs/common.h>

#if GOOGLE_PROTOBUF_VERSION < 3000000
#error This file was generated by a newer version of protoc which is
#error incompatible with your Protocol Buffer headers.  Please update
#error your headers.
#endif
#if 3000000 < GOOGLE_PROTOBUF_MIN_PROTOC_VERSION
#error This file was generated by an older version of protoc which is
#error incompatible with your Protocol Buffer headers.  Please
#error regenerate this file with a newer version of protoc.
#endif

#include <google/protobuf/arena.h>
#include <google/protobuf/arenastring.h>
#include <google/protobuf/generated_message_util.h>
#include <google/protobuf/metadata.h>
#include <google/protobuf/message.h>
#include <google/protobuf/repeated_field.h>
#include <google/protobuf/extension_set.h>
#include <google/protobuf/generated_enum_reflection.h>
#include <google/protobuf/unknown_field_set.h>
// @@protoc_insertion_point(includes)

namespace ShapeProto {

// Internal implementation detail -- do not call these.
void protobuf_AddDesc_ShapeMessage_2eproto();
void protobuf_AssignDesc_ShapeMessage_2eproto();
void protobuf_ShutdownFile_ShapeMessage_2eproto();

class PointMessage;
class PointsMessage;
class ShapeMessage;
class ShapesArray;

enum ShapeMessage_DragAdjustMode {
  ShapeMessage_DragAdjustMode_FREE = 0,
  ShapeMessage_DragAdjustMode_HORI = 1,
  ShapeMessage_DragAdjustMode_VERT = 2,
  ShapeMessage_DragAdjustMode_ShapeMessage_DragAdjustMode_INT_MIN_SENTINEL_DO_NOT_USE_ = ::google::protobuf::kint32min,
  ShapeMessage_DragAdjustMode_ShapeMessage_DragAdjustMode_INT_MAX_SENTINEL_DO_NOT_USE_ = ::google::protobuf::kint32max
};
bool ShapeMessage_DragAdjustMode_IsValid(int value);
const ShapeMessage_DragAdjustMode ShapeMessage_DragAdjustMode_DragAdjustMode_MIN = ShapeMessage_DragAdjustMode_FREE;
const ShapeMessage_DragAdjustMode ShapeMessage_DragAdjustMode_DragAdjustMode_MAX = ShapeMessage_DragAdjustMode_VERT;
const int ShapeMessage_DragAdjustMode_DragAdjustMode_ARRAYSIZE = ShapeMessage_DragAdjustMode_DragAdjustMode_MAX + 1;

const ::google::protobuf::EnumDescriptor* ShapeMessage_DragAdjustMode_descriptor();
inline const ::std::string& ShapeMessage_DragAdjustMode_Name(ShapeMessage_DragAdjustMode value) {
  return ::google::protobuf::internal::NameOfEnum(
    ShapeMessage_DragAdjustMode_descriptor(), value);
}
inline bool ShapeMessage_DragAdjustMode_Parse(
    const ::std::string& name, ShapeMessage_DragAdjustMode* value) {
  return ::google::protobuf::internal::ParseNamedEnum<ShapeMessage_DragAdjustMode>(
    ShapeMessage_DragAdjustMode_descriptor(), name, value);
}
// ===================================================================

class ShapeMessage : public ::google::protobuf::Message {
 public:
  ShapeMessage();
  virtual ~ShapeMessage();

  ShapeMessage(const ShapeMessage& from);

  inline ShapeMessage& operator=(const ShapeMessage& from) {
    CopyFrom(from);
    return *this;
  }

  static const ::google::protobuf::Descriptor* descriptor();
  static const ShapeMessage& default_instance();

  void Swap(ShapeMessage* other);

  // implements Message ----------------------------------------------

  inline ShapeMessage* New() const { return New(NULL); }

  ShapeMessage* New(::google::protobuf::Arena* arena) const;
  void CopyFrom(const ::google::protobuf::Message& from);
  void MergeFrom(const ::google::protobuf::Message& from);
  void CopyFrom(const ShapeMessage& from);
  void MergeFrom(const ShapeMessage& from);
  void Clear();
  bool IsInitialized() const;

  int ByteSize() const;
  bool MergePartialFromCodedStream(
      ::google::protobuf::io::CodedInputStream* input);
  void SerializeWithCachedSizes(
      ::google::protobuf::io::CodedOutputStream* output) const;
  ::google::protobuf::uint8* SerializeWithCachedSizesToArray(::google::protobuf::uint8* output) const;
  int GetCachedSize() const { return _cached_size_; }
  private:
  void SharedCtor();
  void SharedDtor();
  void SetCachedSize(int size) const;
  void InternalSwap(ShapeMessage* other);
  private:
  inline ::google::protobuf::Arena* GetArenaNoVirtual() const {
    return _internal_metadata_.arena();
  }
  inline void* MaybeArenaPtr() const {
    return _internal_metadata_.raw_arena_ptr();
  }
  public:

  ::google::protobuf::Metadata GetMetadata() const;

  // nested types ----------------------------------------------------

  typedef ShapeMessage_DragAdjustMode DragAdjustMode;
  static const DragAdjustMode FREE =
    ShapeMessage_DragAdjustMode_FREE;
  static const DragAdjustMode HORI =
    ShapeMessage_DragAdjustMode_HORI;
  static const DragAdjustMode VERT =
    ShapeMessage_DragAdjustMode_VERT;
  static inline bool DragAdjustMode_IsValid(int value) {
    return ShapeMessage_DragAdjustMode_IsValid(value);
  }
  static const DragAdjustMode DragAdjustMode_MIN =
    ShapeMessage_DragAdjustMode_DragAdjustMode_MIN;
  static const DragAdjustMode DragAdjustMode_MAX =
    ShapeMessage_DragAdjustMode_DragAdjustMode_MAX;
  static const int DragAdjustMode_ARRAYSIZE =
    ShapeMessage_DragAdjustMode_DragAdjustMode_ARRAYSIZE;
  static inline const ::google::protobuf::EnumDescriptor*
  DragAdjustMode_descriptor() {
    return ShapeMessage_DragAdjustMode_descriptor();
  }
  static inline const ::std::string& DragAdjustMode_Name(DragAdjustMode value) {
    return ShapeMessage_DragAdjustMode_Name(value);
  }
  static inline bool DragAdjustMode_Parse(const ::std::string& name,
      DragAdjustMode* value) {
    return ShapeMessage_DragAdjustMode_Parse(name, value);
  }

  // accessors -------------------------------------------------------

  // optional string type = 1;
  void clear_type();
  static const int kTypeFieldNumber = 1;
  const ::std::string& type() const;
  void set_type(const ::std::string& value);
  void set_type(const char* value);
  void set_type(const char* value, size_t size);
  ::std::string* mutable_type();
  ::std::string* release_type();
  void set_allocated_type(::std::string* type);

  // optional string name = 2;
  void clear_name();
  static const int kNameFieldNumber = 2;
  const ::std::string& name() const;
  void set_name(const ::std::string& value);
  void set_name(const char* value);
  void set_name(const char* value, size_t size);
  ::std::string* mutable_name();
  ::std::string* release_name();
  void set_allocated_name(::std::string* name);

  // optional int32 groupid = 3;
  void clear_groupid();
  static const int kGroupidFieldNumber = 3;
  ::google::protobuf::int32 groupid() const;
  void set_groupid(::google::protobuf::int32 value);

  // optional int32 subindex = 4;
  void clear_subindex();
  static const int kSubindexFieldNumber = 4;
  ::google::protobuf::int32 subindex() const;
  void set_subindex(::google::protobuf::int32 value);

  // optional int32 color = 5;
  void clear_color();
  static const int kColorFieldNumber = 5;
  ::google::protobuf::int32 color() const;
  void set_color(::google::protobuf::int32 value);

  // optional .ShapeProto.PointsMessage points = 6;
  bool has_points() const;
  void clear_points();
  static const int kPointsFieldNumber = 6;
  const ::ShapeProto::PointsMessage& points() const;
  ::ShapeProto::PointsMessage* mutable_points();
  ::ShapeProto::PointsMessage* release_points();
  void set_allocated_points(::ShapeProto::PointsMessage* points);

  // optional bool enable = 7;
  void clear_enable();
  static const int kEnableFieldNumber = 7;
  bool enable() const;
  void set_enable(bool value);

  // optional bool showing = 8;
  void clear_showing();
  static const int kShowingFieldNumber = 8;
  bool showing() const;
  void set_showing(bool value);

  // optional .ShapeProto.ShapeMessage.DragAdjustMode dragmode = 9;
  void clear_dragmode();
  static const int kDragmodeFieldNumber = 9;
  ::ShapeProto::ShapeMessage_DragAdjustMode dragmode() const;
  void set_dragmode(::ShapeProto::ShapeMessage_DragAdjustMode value);

  // optional bool showcornorid = 10;
  void clear_showcornorid();
  static const int kShowcornoridFieldNumber = 10;
  bool showcornorid() const;
  void set_showcornorid(bool value);

  // @@protoc_insertion_point(class_scope:ShapeProto.ShapeMessage)
 private:

  ::google::protobuf::internal::InternalMetadataWithArena _internal_metadata_;
  bool _is_default_instance_;
  ::google::protobuf::internal::ArenaStringPtr type_;
  ::google::protobuf::internal::ArenaStringPtr name_;
  ::google::protobuf::int32 groupid_;
  ::google::protobuf::int32 subindex_;
  ::ShapeProto::PointsMessage* points_;
  ::google::protobuf::int32 color_;
  bool enable_;
  bool showing_;
  bool showcornorid_;
  int dragmode_;
  mutable int _cached_size_;
  friend void  protobuf_AddDesc_ShapeMessage_2eproto();
  friend void protobuf_AssignDesc_ShapeMessage_2eproto();
  friend void protobuf_ShutdownFile_ShapeMessage_2eproto();

  void InitAsDefaultInstance();
  static ShapeMessage* default_instance_;
};
// -------------------------------------------------------------------

class PointMessage : public ::google::protobuf::Message {
 public:
  PointMessage();
  virtual ~PointMessage();

  PointMessage(const PointMessage& from);

  inline PointMessage& operator=(const PointMessage& from) {
    CopyFrom(from);
    return *this;
  }

  static const ::google::protobuf::Descriptor* descriptor();
  static const PointMessage& default_instance();

  void Swap(PointMessage* other);

  // implements Message ----------------------------------------------

  inline PointMessage* New() const { return New(NULL); }

  PointMessage* New(::google::protobuf::Arena* arena) const;
  void CopyFrom(const ::google::protobuf::Message& from);
  void MergeFrom(const ::google::protobuf::Message& from);
  void CopyFrom(const PointMessage& from);
  void MergeFrom(const PointMessage& from);
  void Clear();
  bool IsInitialized() const;

  int ByteSize() const;
  bool MergePartialFromCodedStream(
      ::google::protobuf::io::CodedInputStream* input);
  void SerializeWithCachedSizes(
      ::google::protobuf::io::CodedOutputStream* output) const;
  ::google::protobuf::uint8* SerializeWithCachedSizesToArray(::google::protobuf::uint8* output) const;
  int GetCachedSize() const { return _cached_size_; }
  private:
  void SharedCtor();
  void SharedDtor();
  void SetCachedSize(int size) const;
  void InternalSwap(PointMessage* other);
  private:
  inline ::google::protobuf::Arena* GetArenaNoVirtual() const {
    return _internal_metadata_.arena();
  }
  inline void* MaybeArenaPtr() const {
    return _internal_metadata_.raw_arena_ptr();
  }
  public:

  ::google::protobuf::Metadata GetMetadata() const;

  // nested types ----------------------------------------------------

  // accessors -------------------------------------------------------

  // optional float x = 1;
  void clear_x();
  static const int kXFieldNumber = 1;
  float x() const;
  void set_x(float value);

  // optional float y = 2;
  void clear_y();
  static const int kYFieldNumber = 2;
  float y() const;
  void set_y(float value);

  // @@protoc_insertion_point(class_scope:ShapeProto.PointMessage)
 private:

  ::google::protobuf::internal::InternalMetadataWithArena _internal_metadata_;
  bool _is_default_instance_;
  float x_;
  float y_;
  mutable int _cached_size_;
  friend void  protobuf_AddDesc_ShapeMessage_2eproto();
  friend void protobuf_AssignDesc_ShapeMessage_2eproto();
  friend void protobuf_ShutdownFile_ShapeMessage_2eproto();

  void InitAsDefaultInstance();
  static PointMessage* default_instance_;
};
// -------------------------------------------------------------------

class PointsMessage : public ::google::protobuf::Message {
 public:
  PointsMessage();
  virtual ~PointsMessage();

  PointsMessage(const PointsMessage& from);

  inline PointsMessage& operator=(const PointsMessage& from) {
    CopyFrom(from);
    return *this;
  }

  static const ::google::protobuf::Descriptor* descriptor();
  static const PointsMessage& default_instance();

  void Swap(PointsMessage* other);

  // implements Message ----------------------------------------------

  inline PointsMessage* New() const { return New(NULL); }

  PointsMessage* New(::google::protobuf::Arena* arena) const;
  void CopyFrom(const ::google::protobuf::Message& from);
  void MergeFrom(const ::google::protobuf::Message& from);
  void CopyFrom(const PointsMessage& from);
  void MergeFrom(const PointsMessage& from);
  void Clear();
  bool IsInitialized() const;

  int ByteSize() const;
  bool MergePartialFromCodedStream(
      ::google::protobuf::io::CodedInputStream* input);
  void SerializeWithCachedSizes(
      ::google::protobuf::io::CodedOutputStream* output) const;
  ::google::protobuf::uint8* SerializeWithCachedSizesToArray(::google::protobuf::uint8* output) const;
  int GetCachedSize() const { return _cached_size_; }
  private:
  void SharedCtor();
  void SharedDtor();
  void SetCachedSize(int size) const;
  void InternalSwap(PointsMessage* other);
  private:
  inline ::google::protobuf::Arena* GetArenaNoVirtual() const {
    return _internal_metadata_.arena();
  }
  inline void* MaybeArenaPtr() const {
    return _internal_metadata_.raw_arena_ptr();
  }
  public:

  ::google::protobuf::Metadata GetMetadata() const;

  // nested types ----------------------------------------------------

  // accessors -------------------------------------------------------

  // repeated .ShapeProto.PointMessage point = 1;
  int point_size() const;
  void clear_point();
  static const int kPointFieldNumber = 1;
  const ::ShapeProto::PointMessage& point(int index) const;
  ::ShapeProto::PointMessage* mutable_point(int index);
  ::ShapeProto::PointMessage* add_point();
  ::google::protobuf::RepeatedPtrField< ::ShapeProto::PointMessage >*
      mutable_point();
  const ::google::protobuf::RepeatedPtrField< ::ShapeProto::PointMessage >&
      point() const;

  // @@protoc_insertion_point(class_scope:ShapeProto.PointsMessage)
 private:

  ::google::protobuf::internal::InternalMetadataWithArena _internal_metadata_;
  bool _is_default_instance_;
  ::google::protobuf::RepeatedPtrField< ::ShapeProto::PointMessage > point_;
  mutable int _cached_size_;
  friend void  protobuf_AddDesc_ShapeMessage_2eproto();
  friend void protobuf_AssignDesc_ShapeMessage_2eproto();
  friend void protobuf_ShutdownFile_ShapeMessage_2eproto();

  void InitAsDefaultInstance();
  static PointsMessage* default_instance_;
};
// -------------------------------------------------------------------

class ShapesArray : public ::google::protobuf::Message {
 public:
  ShapesArray();
  virtual ~ShapesArray();

  ShapesArray(const ShapesArray& from);

  inline ShapesArray& operator=(const ShapesArray& from) {
    CopyFrom(from);
    return *this;
  }

  static const ::google::protobuf::Descriptor* descriptor();
  static const ShapesArray& default_instance();

  void Swap(ShapesArray* other);

  // implements Message ----------------------------------------------

  inline ShapesArray* New() const { return New(NULL); }

  ShapesArray* New(::google::protobuf::Arena* arena) const;
  void CopyFrom(const ::google::protobuf::Message& from);
  void MergeFrom(const ::google::protobuf::Message& from);
  void CopyFrom(const ShapesArray& from);
  void MergeFrom(const ShapesArray& from);
  void Clear();
  bool IsInitialized() const;

  int ByteSize() const;
  bool MergePartialFromCodedStream(
      ::google::protobuf::io::CodedInputStream* input);
  void SerializeWithCachedSizes(
      ::google::protobuf::io::CodedOutputStream* output) const;
  ::google::protobuf::uint8* SerializeWithCachedSizesToArray(::google::protobuf::uint8* output) const;
  int GetCachedSize() const { return _cached_size_; }
  private:
  void SharedCtor();
  void SharedDtor();
  void SetCachedSize(int size) const;
  void InternalSwap(ShapesArray* other);
  private:
  inline ::google::protobuf::Arena* GetArenaNoVirtual() const {
    return _internal_metadata_.arena();
  }
  inline void* MaybeArenaPtr() const {
    return _internal_metadata_.raw_arena_ptr();
  }
  public:

  ::google::protobuf::Metadata GetMetadata() const;

  // nested types ----------------------------------------------------

  // accessors -------------------------------------------------------

  // repeated .ShapeProto.ShapeMessage shapes = 1;
  int shapes_size() const;
  void clear_shapes();
  static const int kShapesFieldNumber = 1;
  const ::ShapeProto::ShapeMessage& shapes(int index) const;
  ::ShapeProto::ShapeMessage* mutable_shapes(int index);
  ::ShapeProto::ShapeMessage* add_shapes();
  ::google::protobuf::RepeatedPtrField< ::ShapeProto::ShapeMessage >*
      mutable_shapes();
  const ::google::protobuf::RepeatedPtrField< ::ShapeProto::ShapeMessage >&
      shapes() const;

  // @@protoc_insertion_point(class_scope:ShapeProto.ShapesArray)
 private:

  ::google::protobuf::internal::InternalMetadataWithArena _internal_metadata_;
  bool _is_default_instance_;
  ::google::protobuf::RepeatedPtrField< ::ShapeProto::ShapeMessage > shapes_;
  mutable int _cached_size_;
  friend void  protobuf_AddDesc_ShapeMessage_2eproto();
  friend void protobuf_AssignDesc_ShapeMessage_2eproto();
  friend void protobuf_ShutdownFile_ShapeMessage_2eproto();

  void InitAsDefaultInstance();
  static ShapesArray* default_instance_;
};
// ===================================================================


// ===================================================================

#if !PROTOBUF_INLINE_NOT_IN_HEADERS
// ShapeMessage

// optional string type = 1;
inline void ShapeMessage::clear_type() {
  type_.ClearToEmptyNoArena(&::google::protobuf::internal::GetEmptyStringAlreadyInited());
}
inline const ::std::string& ShapeMessage::type() const {
  // @@protoc_insertion_point(field_get:ShapeProto.ShapeMessage.type)
  return type_.GetNoArena(&::google::protobuf::internal::GetEmptyStringAlreadyInited());
}
inline void ShapeMessage::set_type(const ::std::string& value) {
  
  type_.SetNoArena(&::google::protobuf::internal::GetEmptyStringAlreadyInited(), value);
  // @@protoc_insertion_point(field_set:ShapeProto.ShapeMessage.type)
}
inline void ShapeMessage::set_type(const char* value) {
  
  type_.SetNoArena(&::google::protobuf::internal::GetEmptyStringAlreadyInited(), ::std::string(value));
  // @@protoc_insertion_point(field_set_char:ShapeProto.ShapeMessage.type)
}
inline void ShapeMessage::set_type(const char* value, size_t size) {
  
  type_.SetNoArena(&::google::protobuf::internal::GetEmptyStringAlreadyInited(),
      ::std::string(reinterpret_cast<const char*>(value), size));
  // @@protoc_insertion_point(field_set_pointer:ShapeProto.ShapeMessage.type)
}
inline ::std::string* ShapeMessage::mutable_type() {
  
  // @@protoc_insertion_point(field_mutable:ShapeProto.ShapeMessage.type)
  return type_.MutableNoArena(&::google::protobuf::internal::GetEmptyStringAlreadyInited());
}
inline ::std::string* ShapeMessage::release_type() {
  // @@protoc_insertion_point(field_release:ShapeProto.ShapeMessage.type)
  
  return type_.ReleaseNoArena(&::google::protobuf::internal::GetEmptyStringAlreadyInited());
}
inline void ShapeMessage::set_allocated_type(::std::string* type) {
  if (type != NULL) {
    
  } else {
    
  }
  type_.SetAllocatedNoArena(&::google::protobuf::internal::GetEmptyStringAlreadyInited(), type);
  // @@protoc_insertion_point(field_set_allocated:ShapeProto.ShapeMessage.type)
}

// optional string name = 2;
inline void ShapeMessage::clear_name() {
  name_.ClearToEmptyNoArena(&::google::protobuf::internal::GetEmptyStringAlreadyInited());
}
inline const ::std::string& ShapeMessage::name() const {
  // @@protoc_insertion_point(field_get:ShapeProto.ShapeMessage.name)
  return name_.GetNoArena(&::google::protobuf::internal::GetEmptyStringAlreadyInited());
}
inline void ShapeMessage::set_name(const ::std::string& value) {
  
  name_.SetNoArena(&::google::protobuf::internal::GetEmptyStringAlreadyInited(), value);
  // @@protoc_insertion_point(field_set:ShapeProto.ShapeMessage.name)
}
inline void ShapeMessage::set_name(const char* value) {
  
  name_.SetNoArena(&::google::protobuf::internal::GetEmptyStringAlreadyInited(), ::std::string(value));
  // @@protoc_insertion_point(field_set_char:ShapeProto.ShapeMessage.name)
}
inline void ShapeMessage::set_name(const char* value, size_t size) {
  
  name_.SetNoArena(&::google::protobuf::internal::GetEmptyStringAlreadyInited(),
      ::std::string(reinterpret_cast<const char*>(value), size));
  // @@protoc_insertion_point(field_set_pointer:ShapeProto.ShapeMessage.name)
}
inline ::std::string* ShapeMessage::mutable_name() {
  
  // @@protoc_insertion_point(field_mutable:ShapeProto.ShapeMessage.name)
  return name_.MutableNoArena(&::google::protobuf::internal::GetEmptyStringAlreadyInited());
}
inline ::std::string* ShapeMessage::release_name() {
  // @@protoc_insertion_point(field_release:ShapeProto.ShapeMessage.name)
  
  return name_.ReleaseNoArena(&::google::protobuf::internal::GetEmptyStringAlreadyInited());
}
inline void ShapeMessage::set_allocated_name(::std::string* name) {
  if (name != NULL) {
    
  } else {
    
  }
  name_.SetAllocatedNoArena(&::google::protobuf::internal::GetEmptyStringAlreadyInited(), name);
  // @@protoc_insertion_point(field_set_allocated:ShapeProto.ShapeMessage.name)
}

// optional int32 groupid = 3;
inline void ShapeMessage::clear_groupid() {
  groupid_ = 0;
}
inline ::google::protobuf::int32 ShapeMessage::groupid() const {
  // @@protoc_insertion_point(field_get:ShapeProto.ShapeMessage.groupid)
  return groupid_;
}
inline void ShapeMessage::set_groupid(::google::protobuf::int32 value) {
  
  groupid_ = value;
  // @@protoc_insertion_point(field_set:ShapeProto.ShapeMessage.groupid)
}

// optional int32 subindex = 4;
inline void ShapeMessage::clear_subindex() {
  subindex_ = 0;
}
inline ::google::protobuf::int32 ShapeMessage::subindex() const {
  // @@protoc_insertion_point(field_get:ShapeProto.ShapeMessage.subindex)
  return subindex_;
}
inline void ShapeMessage::set_subindex(::google::protobuf::int32 value) {
  
  subindex_ = value;
  // @@protoc_insertion_point(field_set:ShapeProto.ShapeMessage.subindex)
}

// optional int32 color = 5;
inline void ShapeMessage::clear_color() {
  color_ = 0;
}
inline ::google::protobuf::int32 ShapeMessage::color() const {
  // @@protoc_insertion_point(field_get:ShapeProto.ShapeMessage.color)
  return color_;
}
inline void ShapeMessage::set_color(::google::protobuf::int32 value) {
  
  color_ = value;
  // @@protoc_insertion_point(field_set:ShapeProto.ShapeMessage.color)
}

// optional .ShapeProto.PointsMessage points = 6;
inline bool ShapeMessage::has_points() const {
  return !_is_default_instance_ && points_ != NULL;
}
inline void ShapeMessage::clear_points() {
  if (GetArenaNoVirtual() == NULL && points_ != NULL) delete points_;
  points_ = NULL;
}
inline const ::ShapeProto::PointsMessage& ShapeMessage::points() const {
  // @@protoc_insertion_point(field_get:ShapeProto.ShapeMessage.points)
  return points_ != NULL ? *points_ : *default_instance_->points_;
}
inline ::ShapeProto::PointsMessage* ShapeMessage::mutable_points() {
  
  if (points_ == NULL) {
    points_ = new ::ShapeProto::PointsMessage;
  }
  // @@protoc_insertion_point(field_mutable:ShapeProto.ShapeMessage.points)
  return points_;
}
inline ::ShapeProto::PointsMessage* ShapeMessage::release_points() {
  // @@protoc_insertion_point(field_release:ShapeProto.ShapeMessage.points)
  
  ::ShapeProto::PointsMessage* temp = points_;
  points_ = NULL;
  return temp;
}
inline void ShapeMessage::set_allocated_points(::ShapeProto::PointsMessage* points) {
  delete points_;
  points_ = points;
  if (points) {
    
  } else {
    
  }
  // @@protoc_insertion_point(field_set_allocated:ShapeProto.ShapeMessage.points)
}

// optional bool enable = 7;
inline void ShapeMessage::clear_enable() {
  enable_ = false;
}
inline bool ShapeMessage::enable() const {
  // @@protoc_insertion_point(field_get:ShapeProto.ShapeMessage.enable)
  return enable_;
}
inline void ShapeMessage::set_enable(bool value) {
  
  enable_ = value;
  // @@protoc_insertion_point(field_set:ShapeProto.ShapeMessage.enable)
}

// optional bool showing = 8;
inline void ShapeMessage::clear_showing() {
  showing_ = false;
}
inline bool ShapeMessage::showing() const {
  // @@protoc_insertion_point(field_get:ShapeProto.ShapeMessage.showing)
  return showing_;
}
inline void ShapeMessage::set_showing(bool value) {
  
  showing_ = value;
  // @@protoc_insertion_point(field_set:ShapeProto.ShapeMessage.showing)
}

// optional .ShapeProto.ShapeMessage.DragAdjustMode dragmode = 9;
inline void ShapeMessage::clear_dragmode() {
  dragmode_ = 0;
}
inline ::ShapeProto::ShapeMessage_DragAdjustMode ShapeMessage::dragmode() const {
  // @@protoc_insertion_point(field_get:ShapeProto.ShapeMessage.dragmode)
  return static_cast< ::ShapeProto::ShapeMessage_DragAdjustMode >(dragmode_);
}
inline void ShapeMessage::set_dragmode(::ShapeProto::ShapeMessage_DragAdjustMode value) {
  
  dragmode_ = value;
  // @@protoc_insertion_point(field_set:ShapeProto.ShapeMessage.dragmode)
}

// optional bool showcornorid = 10;
inline void ShapeMessage::clear_showcornorid() {
  showcornorid_ = false;
}
inline bool ShapeMessage::showcornorid() const {
  // @@protoc_insertion_point(field_get:ShapeProto.ShapeMessage.showcornorid)
  return showcornorid_;
}
inline void ShapeMessage::set_showcornorid(bool value) {
  
  showcornorid_ = value;
  // @@protoc_insertion_point(field_set:ShapeProto.ShapeMessage.showcornorid)
}

// -------------------------------------------------------------------

// PointMessage

// optional float x = 1;
inline void PointMessage::clear_x() {
  x_ = 0;
}
inline float PointMessage::x() const {
  // @@protoc_insertion_point(field_get:ShapeProto.PointMessage.x)
  return x_;
}
inline void PointMessage::set_x(float value) {
  
  x_ = value;
  // @@protoc_insertion_point(field_set:ShapeProto.PointMessage.x)
}

// optional float y = 2;
inline void PointMessage::clear_y() {
  y_ = 0;
}
inline float PointMessage::y() const {
  // @@protoc_insertion_point(field_get:ShapeProto.PointMessage.y)
  return y_;
}
inline void PointMessage::set_y(float value) {
  
  y_ = value;
  // @@protoc_insertion_point(field_set:ShapeProto.PointMessage.y)
}

// -------------------------------------------------------------------

// PointsMessage

// repeated .ShapeProto.PointMessage point = 1;
inline int PointsMessage::point_size() const {
  return point_.size();
}
inline void PointsMessage::clear_point() {
  point_.Clear();
}
inline const ::ShapeProto::PointMessage& PointsMessage::point(int index) const {
  // @@protoc_insertion_point(field_get:ShapeProto.PointsMessage.point)
  return point_.Get(index);
}
inline ::ShapeProto::PointMessage* PointsMessage::mutable_point(int index) {
  // @@protoc_insertion_point(field_mutable:ShapeProto.PointsMessage.point)
  return point_.Mutable(index);
}
inline ::ShapeProto::PointMessage* PointsMessage::add_point() {
  // @@protoc_insertion_point(field_add:ShapeProto.PointsMessage.point)
  return point_.Add();
}
inline ::google::protobuf::RepeatedPtrField< ::ShapeProto::PointMessage >*
PointsMessage::mutable_point() {
  // @@protoc_insertion_point(field_mutable_list:ShapeProto.PointsMessage.point)
  return &point_;
}
inline const ::google::protobuf::RepeatedPtrField< ::ShapeProto::PointMessage >&
PointsMessage::point() const {
  // @@protoc_insertion_point(field_list:ShapeProto.PointsMessage.point)
  return point_;
}

// -------------------------------------------------------------------

// ShapesArray

// repeated .ShapeProto.ShapeMessage shapes = 1;
inline int ShapesArray::shapes_size() const {
  return shapes_.size();
}
inline void ShapesArray::clear_shapes() {
  shapes_.Clear();
}
inline const ::ShapeProto::ShapeMessage& ShapesArray::shapes(int index) const {
  // @@protoc_insertion_point(field_get:ShapeProto.ShapesArray.shapes)
  return shapes_.Get(index);
}
inline ::ShapeProto::ShapeMessage* ShapesArray::mutable_shapes(int index) {
  // @@protoc_insertion_point(field_mutable:ShapeProto.ShapesArray.shapes)
  return shapes_.Mutable(index);
}
inline ::ShapeProto::ShapeMessage* ShapesArray::add_shapes() {
  // @@protoc_insertion_point(field_add:ShapeProto.ShapesArray.shapes)
  return shapes_.Add();
}
inline ::google::protobuf::RepeatedPtrField< ::ShapeProto::ShapeMessage >*
ShapesArray::mutable_shapes() {
  // @@protoc_insertion_point(field_mutable_list:ShapeProto.ShapesArray.shapes)
  return &shapes_;
}
inline const ::google::protobuf::RepeatedPtrField< ::ShapeProto::ShapeMessage >&
ShapesArray::shapes() const {
  // @@protoc_insertion_point(field_list:ShapeProto.ShapesArray.shapes)
  return shapes_;
}

#endif  // !PROTOBUF_INLINE_NOT_IN_HEADERS
// -------------------------------------------------------------------

// -------------------------------------------------------------------

// -------------------------------------------------------------------


// @@protoc_insertion_point(namespace_scope)

}  // namespace ShapeProto

#ifndef SWIG
namespace google {
namespace protobuf {

template <> struct is_proto_enum< ::ShapeProto::ShapeMessage_DragAdjustMode> : ::google::protobuf::internal::true_type {};
template <>
inline const EnumDescriptor* GetEnumDescriptor< ::ShapeProto::ShapeMessage_DragAdjustMode>() {
  return ::ShapeProto::ShapeMessage_DragAdjustMode_descriptor();
}

}  // namespace protobuf
}  // namespace google
#endif  // SWIG

// @@protoc_insertion_point(global_scope)
#endif

#endif  // PROTOBUF_ShapeMessage_2eproto__INCLUDED
