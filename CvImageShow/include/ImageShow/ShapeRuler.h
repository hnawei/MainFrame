﻿#pragma once

#include "ImageShow/CvImageShow.h"
#include "ImageShow/Shapes.h"

namespace CvImageShow {
	namespace Shapes {
		class CShapeRuler : public CShape
		{
		public:
			CShapeRuler(const ShapeDef& param);

			virtual int IsSelected(const cv::Point& ptPos, float fScale) override;

			virtual bool SetPointsIsOk(const std::vector<cv::Point2f>& vPoints) override;
			std::vector<cv::Point2f>& GetPoints() { return m_vPtCorners; }


#ifdef _USE_QT_
			void DrawSelf(QPainter* pPainter, const cv::Point2f& ptOffset, const float fScaleRatio);
#else
			void DrawSelf(Gdiplus::Graphics* pGdiPainter, const cv::Point2f& ptOffset, const float fScaleRatio);
#endif
			virtual void AdjustDragDelta(const cv::Point2f& ptDelta) override;

            void SetPixelRatio(float _x, float _y)
            {
                m_xPixelRatio = _x;
                m_yPixelRatio = _y;
            }

            float GetXPixelRatio()
            {
                return m_xPixelRatio;
            }

            float GetYPixelRatio()
            {
                return m_yPixelRatio;
            }

		private:
			void SetPoints(const std::vector<cv::Point2f>& vPoints);

            float m_xPixelRatio, m_yPixelRatio;  //增加的数据
		};
	}
}

